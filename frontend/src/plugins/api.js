// fill our API base URL according to the found global ENV variables on this machine

let apiBaseUrl = ''

if (process.env.NODE_ENV === 'development') {
  apiBaseUrl = `http://${process.env.VUE_APP_DOMAIN_DEV}`
} else if (process.env.VUE_APP_ENV === 'staging') {
  apiBaseUrl = `http://${process.env.VUE_APP_DOMAIN_STAG}`
} else {
  apiBaseUrl = `https://${process.env.VUE_APP_DOMAIN_PROD}`
}

export const apiVersion = 'v1'
export const apiUrl = apiBaseUrl + '/api/' + apiVersion + '/'

export default { apiBaseUrl, apiVersion, apiUrl }
