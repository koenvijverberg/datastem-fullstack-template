import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "default" */ '../views/Login.vue'),
    meta: { title: 'Login' }
  },
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: { title: 'Home' }
  },
  {
    path: '/departments',
    name: 'Departments',
    component: () => import(/* webpackChunkName: "departments" */ '../views/Departments.vue'),
    meta: { title: 'Afdelingen' }
  },
  {
    path: '/patients',
    name: 'Patients',
    component: () => import(/* webpackChunkName: "patients" */ '../views/Patients.vue'),
    meta: { title: 'Patiënten' }
  },
  {
    path: '/agenda',
    name: 'Agenda',
    component: () => import(/* webpackChunkName: "agenda" */ '../views/Agenda.vue'),
    meta: { title: 'Agenda' }
  },
  {
    path: '/patient/:id',
    name: 'Patient',
    component: () => import(/* webpackChunkName: "patient" */ '../components/pages/patient/Patient.vue'),
    meta: { title: 'Patiënt' }
  }
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  routes
})

export default router
