export default {
  loading: false,
  patient: {
    patientNumber: 3746263,
    name: 'Jassie Fab',
    dateOfBirth: '1994-10-12',
    reanimationPolicy: 'Volledig beleid',
    bloodType: 'A-',
    location: {
      departmentId: 3,
      department: 'Interne Geneeskunde',
      locationInDepartment: 'Kamer 5, bed 9'
    },
    leadDoctor: {
      id: 8282,
      name: 'Koen Vijverberg',
      phoneNumber: '065149584'
    },
    address: {
      residence: 'Nimma Cityyy',
      street: 'Verlengde Statenlaan',
      houseNumber: 41,
      houseNumberExt: 'B',
      zipCode: '5223 LD'
    },
    sex: 'Female',
    socialSecNr: '29357392',
    generalPractitioner: {
      name: 'Huisartsenpraktijk Den Bosch',
      address: 'Verlengde Statenlaan 1',
      phoneNumber: '070-018382929'
    },
    pharmacist: 'Pillenfabriek in Oss'
  },
  patients: [
    {
      patientNumber: 85939281,
      name: 'John Doe',
      dateOfBirth: '1980-05-02',
      reanimationPolicy: 'Volledig beleid',
      location: {
        departmentId: 3,
        department: 'Interne Geneeskunde',
        locationInDepartment: 'Kamer 5, bed 7'
      },
      leadDoctor: {
        id: 8282,
        name: 'Koen Vijverberg',
        phoneNumber: '065149584'
      },
      address: {
        residence: 'Nimma Cityyy',
        street: 'Verlengde Statenlaan',
        houseNumber: 41,
        houseNumberExt: 'B',
        zipCode: '5223 LD'
      },
      sex: 'Male',
      socialSecNr: '29357392',
      generalPractitioner: 'Huisartsenpraktijk Den Bosch',
      pharmacist: 'Pillenfabriek in Oss'
    },
    {
      patientNumber: 3746263,
      name: 'Jassie Fab',
      dateOfBirth: '1994-10-12',
      reanimationPolicy: 'Volledig beleid',
      location: {
        departmentId: 3,
        department: 'Interne Geneeskunde',
        locationInDepartment: 'Kamer 5, bed 9'
      },
      leadDoctor: {
        id: 8282,
        name: 'Koen Vijverberg',
        phoneNumber: '065149584'
      },
      address: {
        residence: 'Nimma Cityyy',
        street: 'Verlengde Statenlaan',
        houseNumber: 41,
        houseNumberExt: 'B',
        zipCode: '5223 LD'
      },
      sex: 'Female',
      socialSecNr: '29357392',
      generalPractitioner: {
        name: 'Huisartsenpraktijk Den Bosch',
        address: 'Verlengde Statenlaan 1',
        phoneNumber: '070-018382929'
      },
      pharmacist: 'Pillenfabriek in Oss'
    }
  ]
}
