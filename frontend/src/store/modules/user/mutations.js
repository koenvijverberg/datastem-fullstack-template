export default {
  setLoading (state, data) {
    state.loading = data
  },
  setUser (state, data) {
    state.user = data
  }
}
