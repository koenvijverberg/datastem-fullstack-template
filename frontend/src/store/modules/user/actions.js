export default {
  async getUser ({ state, commit }) {
    commit('setLoading', true)
    await state.apiHandle
      .get('/user/', {
        headers: { 'X-Fields': 'id, name, username, email, roles, admin' }
      })
      .then(response => {
        commit('setUser', response.data)
      })
      .catch(() => {
        // getting the user failed
        // possible reasons:
        //  - Invalid token supplied (hacking attempt)
        //  - Token was blacklisted, and thus rejected by the server
        commit('logout')
      })
      .then(commit('setLoading', false))
  }
}
