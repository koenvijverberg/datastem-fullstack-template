#!/bin/sh

# install the backend python files as a module
cd /workspace/backend
pip install -e .

# upgrade the database at startup
alembic upgrade head

# then do an endless sleep loop
while sleep 1000; do :; done