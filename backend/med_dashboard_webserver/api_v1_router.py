from fastapi import APIRouter

from med_dashboard_webserver.controllers import user_controller, auth_controller, common_controller, department_controller

api_router = APIRouter()
api_router.include_router(user_controller.router, prefix="/users", tags=["users"])
api_router.include_router(auth_controller.router, prefix="/auth", tags=["auth"])
api_router.include_router(common_controller.router, prefix="/common", tags=["common"])
api_router.include_router(department_controller.router, prefix="/department", tags=["department"])
