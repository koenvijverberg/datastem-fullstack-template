from sqlalchemy.ext.hybrid import hybrid_property

from med_dashboard_webserver.models import db
from med_dashboard_webserver.models.util import TimestampMixin

class MedicineATCCode(db.Model, TimestampMixin):
    '''Stores medicine ATC codes'''
    __tablename__ = 'medicine_atc'
    # primary key
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    # forein keys
    parent_id = db.Column(db.Integer, db.ForeignKey('medicine_atc.id'), nullable=True)

    # object properties
    name = db.Column(db.UnicodeText, nullable=False, unique=True)

    # relationships
    children = db.relationship("MedicineATCCode", backref=db.backref('parent', remote_side=[id]))


class Medicine(db.Model, TimestampMixin):
    """Stores medicine information"""
    __tablename__ = 'medicine'
    # primary key
    id                          = db.Column(db.Integer, primary_key=True, autoincrement=True)

    # forein keys
    atc_code_id                 = db.Column(db.Integer, db.ForeignKey('medicine_atc.id'), nullable=True)
    supplier_id                 = db.Column(db.Integer, db.ForeignKey('supplier.id'), nullable=True)

    # object properties
    name                        = db.Column(db.UnicodeText, nullable=False)
    name_prescription           = db.Column(db.UnicodeText, nullable=True)
    description                 = db.Column(db.UnicodeText, nullable=True)
    zi_code                     = db.Column(db.Integer, nullable=True)
    country_of_origin           = db.Column(db.UnicodeText, nullable=True)
    price_eur_cents             = db.Column(db.Integer, nullable=True)
    acquire_channel_code        = db.Column(db.Integer, nullable=True)
    registration_emea           = db.Column(db.UnicodeText, nullable=True)
    registration_rvg_1          = db.Column(db.Integer, nullable=True)
    registration_rvg_2          = db.Column(db.Integer, nullable=True)
    registration_rvg_3          = db.Column(db.Integer, nullable=True)

    # relationships 
    atc_code            = db.relationship('MedicineATCCode', lazy='joined', uselist=False)
    supplier            = db.relationship('Supplier', lazy='joined', uselist=False)

    @hybrid_property
    def price(self):
        return float(self.price_eur_cents) / float(100)

    @price.setter
    def price(self, price):
        self.price_eur_cents = int(price * 100)
