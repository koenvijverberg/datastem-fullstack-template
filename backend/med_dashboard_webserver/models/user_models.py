import datetime

import bcrypt
from sqlalchemy import Column, ForeignKey, Boolean, Integer, Unicode, UnicodeText, DateTime, and_, or_, func
from sqlalchemy.orm import relationship, backref

from med_dashboard_webserver.db.base_class import Base
from med_dashboard_webserver.models.util import TimestampMixin, EmailType
from med_dashboard_webserver.models import department_models

class UserDepartmentFavourites(Base):
    __tablename__ = 'user_department_favourites'
    # primary key
    id = Column(Integer, primary_key=True, autoincrement=True)

    # foreign keys
    user_id = Column(Integer, ForeignKey('user.id'), index=True, nullable=False)
    department_id = Column(Integer, ForeignKey('department.id'), index=True, nullable=False)


class Role(Base, TimestampMixin):
    """Stores the roles we have"""
    __tablename__ = 'role'
    # primary key
    id = Column(Integer, primary_key=True, autoincrement=True)

    # forein keys
    parent_id = Column(Integer, ForeignKey('role.id'), nullable=True)

    # object properties
    name = Column(UnicodeText, nullable=False, unique=True)
    description = Column(UnicodeText, nullable=False)

    # relationships
    children = relationship("Role", backref=backref('parent', remote_side=[id]))


class UserRole(Base, TimestampMixin):
    """Store user role"""
    __tablename__ = 'user_role'

    def __init__(self, user=None, role=None, role_start=None, role_end=None):
        self.user = user
        self.role = role
        self.role_start = role_start
        self.role_end = role_end

    # primary key
    id = Column(Integer, primary_key=True, autoincrement=True)

    # foreign keys
    user_id = Column(Integer, ForeignKey('user.id'), index=True, nullable=False)
    role_id = Column(Integer, ForeignKey('role.id'), index=True, nullable=False)

    # object properties
    role_start  = Column(DateTime, index=True, nullable=True)
    role_end    = Column(DateTime, index=True, nullable=True)

    # relationships
    user = relationship('User', backref='role_associations', uselist=False)
    role = relationship('Role', backref='role_associations', uselist=False)


class User(Base, TimestampMixin):
    """Model for storing subcategories under a category"""
    __tablename__ = 'user'
    # primary key
    id              = Column(Integer, primary_key=True, autoincrement=True)

    # object properties
    is_active       = Column(Boolean, nullable=False, server_default='true')
    is_admin        = Column(Boolean, nullable=False, server_default='false')
    username        = Column(UnicodeText, nullable=False, unique=True)
    email           = Column(EmailType, nullable=False, unique=True)
    password_hash   = Column(Unicode(255), nullable=False)


    # relationships
    # slightly complex join -> allow the role start and end dates in the pivot table to be set to null
    # but adhere to the time limits if they are specified
    roles = relationship(
        'Role', 
        secondary='user_role',
        secondaryjoin=and_(
            Role.id == UserRole.role_id,
            or_(UserRole.role_start == None, UserRole.role_start <= func.now()),
            or_(UserRole.role_end == None, UserRole.role_end >= func.now()),
        ),
        lazy='joined'
    )
    department_favourites = relationship(
        'Department',
        secondary='user_department_favourites',
        lazy='joined'
    )

    @property
    def password(self):
        raise AttributeError('password not readable')

    @password.setter
    def password(self, password):
        # bcrypt expects bytes as input
        self.password_hash = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')

    def check_password(self, password):
        # bcrypt expects bytes as input
        return bcrypt.checkpw(password.encode('utf-8'), self.password_hash.encode('utf-8'))
