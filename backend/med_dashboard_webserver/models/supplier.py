from sqlalchemy.ext.hybrid import hybrid_property

from med_dashboard_webserver.models import db
from med_dashboard_webserver.models.util import TimestampMixin

class Supplier(db.Model, TimestampMixin):
    '''Stores info on the suppliers we have'''
    __tablename__ = 'supplier'
    # primary key
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    # object properties
    name = db.Column(db.UnicdeText, nullable=False, unique=True)

