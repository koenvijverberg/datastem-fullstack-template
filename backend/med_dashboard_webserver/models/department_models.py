import datetime

from sqlalchemy import Column, ForeignKey, Integer, UnicodeText
from sqlalchemy.orm import relationship, backref

from med_dashboard_webserver.db.base_class import Base
from med_dashboard_webserver.models.util import TimestampMixin

class Department(Base, TimestampMixin):
    """Stores the departments we have"""
    __tablename__ = 'department'
    # primary key
    id = Column(Integer, primary_key=True, autoincrement=True)

    # forein keys
    parent_id = Column(Integer, ForeignKey('department.id'), nullable=True)

    # object properties
    name = Column(UnicodeText, nullable=False, unique=True)
    description = Column(UnicodeText, nullable=True)

    # relationships
    children = relationship("Department", backref=backref('parent', remote_side=[id]))
