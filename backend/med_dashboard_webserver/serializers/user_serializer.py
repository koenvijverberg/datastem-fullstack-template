from typing import List, Optional
from datetime import datetime

from pydantic import BaseModel, Field, constr

class UserRole(BaseModel):
    id          : int
    parent_id   : Optional[int] = None
    name        : str
    description : str
    # role_start  : datetime
    # role_stop   : datetime

    class Config:
        orm_mode = True


class UserBase(BaseModel):
    email: str

class UserCreate(UserBase):
    password: str

class User(UserBase):
    id          : int
    username    : constr(min_length=3, max_length=50)
    email       : constr(min_length=3, max_length=50)
    roles       : List[UserRole]

    class Config:
        orm_mode = True
