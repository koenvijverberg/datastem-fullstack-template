from typing import List, ForwardRef

from pydantic import BaseModel


Department = ForwardRef('Department')

class Department(BaseModel):
    id          : int
    name        : str
    children    : List[Department]

    class Config:
        orm_mode = True

Department.update_forward_refs()
