from pydantic import BaseModel

from med_dashboard_webserver.serializers import user_serializer


class Msg(BaseModel):
    msg: str

class CommonResponse(BaseModel):
    user: user_serializer.User
