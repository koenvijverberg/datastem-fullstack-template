from datetime import datetime, timedelta

import jwt
from pydantic import ValidationError
from sqlalchemy.orm import Session

from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer

from med_dashboard_webserver.config import config
from med_dashboard_webserver.db.deps import get_db
from med_dashboard_webserver.services import user_service
from med_dashboard_webserver.models import user_models
from med_dashboard_webserver.serializers import auth_serializer

reusable_oauth2 = OAuth2PasswordBearer(
    tokenUrl="/api/v1/auth/login/access-token"
)

def authenticate_user(db: Session, login_request : auth_serializer.UserLoginRequest) -> auth_serializer.Token:
    '''Small function to check if username & password match, and generate JWT token if yes'''
    # check if a user with this username exists
    user = user_service.get_user_by_username(db, login_request.username)
    if user:
        password_ok = user.check_password(login_request.password)
        if password_ok:
            # generate the time when this token will expire
            expiration_date = datetime.utcnow() + timedelta(days=31)
            # generate the token payload data we'll sign
            token_payload = auth_serializer.TokenPayload()
            token_payload.sub = user.id
            token_payload.exp = expiration_date
            # do the signing process using our secret key
            token_data = jwt.encode(dict(token_payload), config.JWT_SECRET_KEY, algorithm='HS256')
            # generate the token we'll send back to the client
            token = auth_serializer.Token(
                access_token=token_data,
                token_type='Bearer'
            )
            return token
    raise HTTPException(
        status_code=400, detail="Incorrect username or password"
    )

def get_current_user(
    db: Session = Depends(get_db), token: str = Depends(reusable_oauth2)
) -> user_models.User:
    try:
        payload = jwt.decode(token, config.JWT_SECRET_KEY, algorithms=['HS256'])
        token_data = auth_serializer.TokenPayload(**payload)
    except (jwt.PyJWTError, ValidationError):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Could not validate credentials",
        )
    user = user_service.get_user(db, user_id=token_data.sub)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    return user


def get_current_active_user(
    current_user: user_models.User = Depends(get_current_user),
) -> user_models.User:
    if not current_user.is_active:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user


def get_current_active_superuser(
    current_user: user_models.User = Depends(get_current_user),
) -> user_models.User:
    if not current_user.is_superuser:
        raise HTTPException(
            status_code=400, detail="The user doesn't have enough privileges"
        )
    return current_user
