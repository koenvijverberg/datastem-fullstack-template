from sqlalchemy.orm import Session

from med_dashboard_webserver.serializers import user_serializer
from med_dashboard_webserver.models import user_models


def get_user(db: Session, user_id: int):
    return db.query(user_models.User).filter(user_models.User.id == user_id).first()

def get_user_by_username(db: Session, username: str):
    return db.query(user_models.User).filter(user_models.User.username == username).first()

def get_user_by_email(db: Session, email: str):
    return db.query(user_models.User).filter(user_models.User.email == email).first()

def get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(user_models.User).offset(skip).limit(limit).all()

def create_user(db: Session, user: user_serializer.UserCreate):
    db_user = user_models.User(**user.dict())
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

def seed_users(db: Session):
    # Create a new Faker and tell it how to create User objects
    user = user_models.User(
        username='koen',
        email='koen@vijverb.nl',
        password='koen',
        is_admin=True,
        is_active=True
    )
    role = user_models.Role(
        name='everyone',
        description='Base role for any logged in user'
    )
    role2 = user_models.Role(
        name='superuser',
        description='Superuser role',
        parent=role
    )
    user.roles.append(role)
    db.add(user)
    db.commit()
