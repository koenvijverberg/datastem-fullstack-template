from med_dashboard_webserver.services.service import BaseService
from med_dashboard_webserver.models.medicine import Medicine


class MedicineService(BaseService):
    pass


medicine_service = MedicineService(Medicine)
