from sqlalchemy.orm import Session

from med_dashboard_webserver.models import department_models

def get_all_departments(db: Session):
    return db.query(department_models.Department).all()
