from med_dashboard_webserver.services.service import BaseService
from med_dashboard_webserver.models.supplier import Supplier


class SupplierService(BaseService):
    pass


supplier_service = SupplierService(Supplier)
