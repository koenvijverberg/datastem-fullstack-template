# Import all the models, so that Base has them before being
# imported by Alembic
from med_dashboard_webserver.db.base_class import Base  # noqa
from med_dashboard_webserver.models.user_models import User  # noqa
