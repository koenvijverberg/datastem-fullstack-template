from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from med_dashboard_webserver.config import config

engine = create_engine(config.SQLALCHEMY_DATABASE_URI, pool_pre_ping=True, connect_args={ 'check_same_thread': False })
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
