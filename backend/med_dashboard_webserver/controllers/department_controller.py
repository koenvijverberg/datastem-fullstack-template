from typing import List

from fastapi import APIRouter, Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session

from med_dashboard_webserver.db.deps import get_db
from med_dashboard_webserver.serializers import department_serializer, user_serializer
from med_dashboard_webserver.services import department_service, auth_service

router = APIRouter()

@router.get("/", response_model=List[department_serializer.Department])
def get_all_departments(
        current_active_user : user_serializer.User = Depends(auth_service.get_current_active_user),
        db: Session = Depends(get_db)
    ):
    return department_service.get_all_departments(db)
