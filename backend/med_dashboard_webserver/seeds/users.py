from med_dashboard_webserver.models.user_models import User, Role

# All seeders inherit from Seeder
class UserSeeder(Seeder):

  # run() will be called by Flask-Seeder
  def run(self):
    # Create a new Faker and tell it how to create User objects
    user = User(
        username='koen',
        email='koen@vijverb.nl',
        password='koen'
    )
    role = Role(
        name='everyone',
        description='Base role for any logged in user'
    )
    role2 = Role(
        name='superuser',
        description='Superuser role',
        parent=role
    )
    user.roles.append(role)
    self.db.session.add(user)
