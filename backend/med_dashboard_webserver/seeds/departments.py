from med_dashboard_webserver.models.department import Department
from flask_seeder import Seeder, Faker, generator

# All seeders inherit from Seeder


class DepartmentSeeder(Seeder):

    departments = [
        {
            'name': 'Interne Geneeskunde',
            'children': [
                {
                    'name': 'Algemene interne'
                },
                {
                    'name': 'Infectieziekten'
                },
                {
                    'name': 'Oncologie'
                },
                {
                    'name': 'Hematologie'
                }
            ]
        },
        {
            'name': 'Spoedeisende Hulp'
        },
        {
            'name': 'Intensive Care'
        },
        {
            'name': 'Medium Care'
        },
        {
            'name': 'Chirurgie'
        },
        {
            'name': 'Gynecologie'
        },
        {
            'name': 'Operatiekamers',
            'children': [
                {
                    'name': 'OK 1'
                },
                {
                    'name': 'OK 2'
                },
                {
                    'name': 'OK 3'
                }
            ]
        }
    ]
    # run() will be called by Flask-Seeder

    def run(self):
        departments = self.create_departments_recursive(self.departments)
        for d in departments:
            # Create a new Faker and tell it how to create User objects
            self.db.session.add(d)


    def create_departments_recursive(self, departments):
        deps = []
        for d in departments:
            has_children = False
            if 'children' in d:
                has_children = True
                child_deps = d['children']
                del d['children']
            dep = Department(**d)
            if has_children:
                dep.children = self.create_departments_recursive(child_deps)
            deps.append(dep)
        return deps
